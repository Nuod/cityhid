package com.example.demo.feing;

import com.example.demo.dto.CityHid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "CityHid", url = "${feign.url}")
public interface CityHidFeign {

    @RequestMapping(method = RequestMethod.GET)
     List<CityHid> getCityHid();
}