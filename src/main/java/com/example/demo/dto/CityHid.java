package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CityHid {
    private String hid;
    @JsonProperty("text")
    private String city;
}
