package com.example.demo.controller;

import com.example.demo.service.CityHidService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class DefaultController {

    private final CityHidService cityHidService;

    @GetMapping("/{cityName}")
    public ResponseEntity getSome(@PathVariable String cityName) {
        return ResponseEntity.ok(cityHidService.getHidByCity(cityName));
    }
}
