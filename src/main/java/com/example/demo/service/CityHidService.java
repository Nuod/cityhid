package com.example.demo.service;

import com.example.demo.dto.CityHid;

public interface CityHidService {
    CityHid getHidByCity(String cityName);
}
