package com.example.demo.service.implementation;

import com.example.demo.dto.CityHid;
import com.example.demo.feing.CityHidFeign;
import com.example.demo.service.CityHidService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class CityHidServiceImpl implements CityHidService {

    private final CityHidFeign cityHidFeign;

    @Override
    @Cacheable("city_hid_cache" )
    public CityHid getHidByCity(String cityName) {
        log.info("do method getHidByCity");
        List<CityHid> cityHidList = cityHidFeign.getCityHid();
        for (CityHid itr : cityHidList) {
            if (itr.getCity().equals(cityName)) {
                return itr;
            }
        }
        return null;
    }
    @Scheduled(fixedDelayString = "${cache.lifetime:10000}")
    @CacheEvict(cacheNames = "city_hid_cache", allEntries = true)
    public void evictCache() {
        log.info("Clear cache");
    }
}
